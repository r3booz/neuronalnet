import java.util.concurrent.TimeUnit;

import NeuronalNetwork.Network;

public class AI {

    public static void main(String[] args) {
    	System.out.println("###");
        System.out.println("Create NeuronalAINetwork (NAIN)");
        double[] hidden = {1.0,2.0,1.0};
        double[] output = {1.0};
        Network NET = new Network(2,1,output,1,hidden,3,0,0);
        //NET.load("C:\test.xml");
        
        
        System.out.println(NET.BoxMuller(0.0, 1.0));
        
        //Manual Connection
        NET.input.get(0).connect(NET.layer.get(0).hidden.get(0),1.0);
        NET.input.get(0).connect(NET.layer.get(0).hidden.get(1),1.0);
        NET.input.get(1).connect(NET.layer.get(0).hidden.get(1),1.0);
        NET.input.get(1).connect(NET.layer.get(0).hidden.get(2),1.0);
        NET.layer.get(0).hidden.get(0).connect(NET.output.get(0),1.0);
        NET.layer.get(0).hidden.get(1).connect(NET.output.get(0),-2.0);
        NET.layer.get(0).hidden.get(2).connect(NET.output.get(0),1.0);
        
        System.out.println("\nCreated with:");
        NET.printStats();
        System.out.println("###");
        
        GUIInterface.GUI gui = new GUIInterface.GUI(NET);
        while(gui.exit!=true)
        {
        	try {
				TimeUnit.MILLISECONDS.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        System.out.println("Learn input...");
        
        //[] = Eingang [] = Ausgang
        boolean ready = false;
        
        double[][] data = {
    			{0.0,0.0,0.0},
    			{1.0,0.0,1.0},
    			{0.0,1.0,1.0},
    			{1.0,1.0,0.0},
    	};
        
        double[] x = data[1];
        System.out.println(String.valueOf(x.length));
        
        ready = NET.learn(data);
        	
        if (ready==false)
        {
        	System.out.println("Learning Error!");
        	//System.exit(-1);
        }
        
        double[] in = {1.0,0.0};
        System.out.println("Compute input: "+in[0]+" "+in[1]);
        double[] out = null;
        out = NET.compute(in);
        System.out.println("Output: "+out[0]);
        System.out.println("###");
    }

}