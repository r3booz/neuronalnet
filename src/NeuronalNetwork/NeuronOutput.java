package NeuronalNetwork;

public class NeuronOutput extends Neuron
{	
	public NeuronOutput(double threshold) {
		super(threshold);
	}
	
	public void connect(NeuronInput neuron, double weight)
	{
		System.out.println("Can�t connect backwards!");
		System.exit(-1);
		Synapse synTemp = new Synapse(neuron,weight);
		synTemp.setIn(this);
		this.synapse.add(synTemp);
		neuron.synapseIn.add(synTemp);
	}
	
	public void connect(NeuronHidden neuron, double weight)
	{
		if (this.getClass().getName()=="NeuronalNetwork.NeuronOutput")
		{
			System.out.println("Can�t connect backwards!");
			System.exit(-1);
		}
		else
		{
			Synapse synTemp = new Synapse(neuron,weight);
			synTemp.setIn(this);
			this.synapse.add(synTemp);
			neuron.synapseIn.add(synTemp);
		}
	}
	
	public void connect(NeuronOutput neuron, double weight)
	{
		Synapse synTemp = new Synapse(neuron,weight);
		synTemp.setIn(this);
		this.synapse.add(synTemp);
		neuron.synapseIn.add(synTemp);
	}
	
	public double fire()
	{
		//Calc
		double out = 0.0;
		double sum = 0.0;
		for(int i = 0; i < this.synapseIn.size(); i++)
		{
			sum += this.synapseIn.get(i).weight * this.synapseIn.get(i).outVal;
			System.out.println("---Synapse "+i+" = "+this.synapseIn.get(i).outVal);
		}
		if (sum >= this.threshold)
			out = 1.0;
		else
			out = 0.0;
		
		return out;
	}
	
}