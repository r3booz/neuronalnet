package NeuronalNetwork;
import java.util.ArrayList;

public class Layer {
	public ArrayList<NeuronHidden> hidden = new ArrayList<NeuronHidden>();
	//public double[] threshold;
	
	public Layer(int hiddenCount, double[] threshold)
	{
		if (threshold.length!=hiddenCount)
		{
			System.out.println("Error hidden.bias.lenght "+threshold.length+":"+hiddenCount);
			System.exit(-1);
		}
		for(int i = 0; i < hiddenCount; i++)
		{
			this.hidden.add(new NeuronHidden(threshold[i]));
		}
	}
}