package NeuronalNetwork;
import java.util.ArrayList;
import java.util.*;

import XMLInterface.XML;


public class Network {
	public ArrayList<NeuronInput> input = new ArrayList<NeuronInput>();
	public ArrayList<Layer> layer = new ArrayList<Layer>();
	public ArrayList<NeuronOutput> output = new ArrayList<NeuronOutput>();
	
	int NeuronInputCount = 0;
	int NeuronLayerCount = 0;
	int NeuronHiddenCount = 0;
	int NeuronOutputCount = 0;
	int SynapseCount = 0;

	public Network(int inputCount, int outputCount, double[] thresholdOutput, int layerCount, double[] thresholdHidden, int hidden1, int hidden2, int hidden3)
	{
		//Create Neurons
		for(int i = 0; i < inputCount; i++)
		{
			this.input.add(new NeuronInput());
			this.NeuronInputCount++;
		}
		if (layerCount > 0)
		{
			this.NeuronLayerCount=layerCount;
			this.layer.add(new Layer(hidden1,thresholdHidden));
			this.NeuronHiddenCount=NeuronHiddenCount+hidden1;
			if (layerCount > 1)
			{
				this.NeuronHiddenCount=NeuronHiddenCount+hidden2;
				this.layer.add(new Layer(hidden2,thresholdHidden));
				if (layerCount > 2)
					this.NeuronHiddenCount=NeuronHiddenCount+hidden3;
					this.layer.add(new Layer(hidden3,thresholdHidden));
			}
		}
		for(int i = 0; i < outputCount; i++)
		{
			this.output.add(new NeuronOutput(thresholdOutput[i]));
			this.NeuronOutputCount++;
		}
	}
	
	public boolean load(String file)
	{
		System.out.println("+Reading "+file);
		XML fileXML = new XML(file);
		return true;
	}
	
	public double[] compute(double[] in)
	{
		/*double[] inputResult = new double[this.input.size()];
		double[] hiddenResult = new double[this.input.size()];
		*/
		
		double[] outputResult = new double[this.output.size()];
		
		for(int i = 0; i < this.input.size(); i++)
		{
			this.input.get(i).outVal = in[i];
			System.out.println("--Compute IN: "+i+" -> "+this.input.get(i).fire());
		}
		for(int il = 0; il < this.layer.size(); il++)
		{
			for(int i = 0; i < this.layer.get(il).hidden.size(); i++)
			{
				System.out.println("--Compute HI: "+i+" -> "+this.layer.get(il).hidden.get(i).fire());
			}
		}
		for(int i = 0; i < this.output.size(); i++)
		{
			outputResult[i] = this.output.get(i).fire();
			System.out.println("--Compute OU: "+i+" -> "+outputResult[i]);
		}
		return outputResult;
	}
	
	public double getNeuronStat()
	{
		return 0.0;
	}
	
	private boolean backpropagation(double[] in)
	{
		double[] outNet = this.compute(in);
		double[] delta = new double[outNet.length];
		/*
		 * length = 1 (2)
		 * NeuronOut = 2
		 * 0 = 1-(2-1)+0
		 */
		for(int i = 0; i < this.NeuronOutputCount; i++)
		{
			delta[i] = Math.abs(outNet[i] - in[(in.length-1)-i]);
			if (delta[i]==0)
				return true;
		}
		//hvu
		return false;
	}
	
	private boolean delta(double[] in)
	{
		return false;
	}
	
	public boolean learn(double[][] data)
	{
		boolean ret = false;
		if (NeuronLayerCount>0)
		{
			for(int i = 0; i < data.length; i++)
			{
				ret = this.backpropagation(data[i])&&ret;
			}
			return ret;
		}
		else
		{
			for(int i = 0; i < data.length; i++)
			{
				ret = this.delta(data[i])&&ret;
			}
			return ret;
		}
	}
	
	////*** GET FUNCTIONS ***////
	public int getNeuronInputCount()
	{
		return this.NeuronInputCount;
	}
	
	public int getNeuronLayerCount()
	{
		return this.NeuronLayerCount;
	}
	
	public int getNeuronHiddenCount()
	{
		return this.NeuronHiddenCount;
	}
	
	public int getNeuronOutputCount()
	{
		return this.NeuronOutputCount;
	}
	
	public void printStats()
	{
		System.out.println("Input  Neurons:"+this.NeuronInputCount);
        System.out.println("Hidden  Layers:"+this.NeuronLayerCount);
        System.out.println("Hidden Neurons:"+this.NeuronHiddenCount);
        System.out.println("Output Neurons:"+this.NeuronOutputCount);
	}
	////*********************////
	
	public double BoxMuller(double m, double s)
	{
	    double x1, x2, w, y1;
	 
	    Random r = new Random();
	    
	    do {
	       x1 = 2.0d * r.nextDouble() - 1.0d;    // NextDouble() is uniform in 0..1
	       x2 = 2.0d * r.nextDouble() - 1.0d;
	       w = x1 * x1 + x2 * x2;
	    } while (w >= 1.0d);
	 
	    w = Math.sqrt((-2.0d * Math.log(w)) / w);
	    y1 = x1 * w;
	    //_y2 = x2 * w;
	    //_useLast = true;
	    
	    double z = Math.abs(m + y1 * s);
	    
	    if (z>1.0)
	    	z -= 1.0;
	    
	    return z;
	}
}
