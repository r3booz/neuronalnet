package NeuronalNetwork;

public class Synapse {
	public int type = -1;
	public NeuronInput input; //inType=0
	public NeuronHidden hidden; //outType=1
	public NeuronOutput output; //outType=2
	public int inId = 0;
	
	public int inType = -1;
	public NeuronInput inInput; //inType=0
	public NeuronHidden inHidden; //outType=1
	public NeuronOutput inOutput; //outType=2
	public int outId = 0;
	
	public double weight = 0.0;
	double outVal = 0.0;
	
	public Synapse(NeuronInput neuron, double weight)
	{
		this.type = 0;
		this.input = neuron;
		this.weight = weight;
		//Network.NeuronInput.this.SynapseCount += 1;
		//Add synapse count
	}
	public Synapse(NeuronHidden neuron, double weight)
	{
		this.type = 1;
		this.hidden = neuron;
		this.weight = weight;
	}
	public Synapse(NeuronOutput neuron, double weight)
	{
		this.type = 2;
		this.output = neuron;
		this.weight = weight;
	}
	/////////////////////////////
	public void setIn(NeuronInput neuron)
	{
		this.inInput = neuron;
	}
	public void setIn(NeuronHidden neuron)
	{
		this.inHidden = neuron;
	}
	public void setIn(NeuronOutput neuron)
	{
		this.inOutput = neuron;
	}
	
	/*public void setOutput(double out)
	{
		switch(this.type)
		{
			case -1:
				//Not connected
				System.out.println("WARNING: Not connected Synapse!");
				System.exit(-1);
			break;
			case 0: //Input
				//Backward
				System.out.println("ERROR: Can�t connect backwards!");
				System.exit(-1);
			break;
			case 1: //Hidden
				this.hidden.inVal += out;
			break;
			case 2: //Output
				this.output.outVal += out;
			break;
		}
	}*/
}