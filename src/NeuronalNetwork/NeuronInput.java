package NeuronalNetwork;

public class NeuronInput extends Neuron
{

	public NeuronInput() {
		super(0.0);
	}
	
	public void connect(NeuronInput neuron, double weight)
	{
		System.out.println("Can�t connect backwards!");
		System.exit(-1);
		Synapse synTemp = new Synapse(neuron,weight);
		synTemp.setIn(this);
		this.synapse.add(synTemp);
		neuron.synapseIn.add(synTemp);
	}
	
	public void connect(NeuronHidden neuron, double weight)
	{
		if (this.getClass().getName()=="NeuronalNetwork.NeuronOutput")
		{
			System.out.println("Can�t connect backwards!");
			System.exit(-1);
		}
		else
		{
			Synapse synTemp = new Synapse(neuron,weight);
			synTemp.setIn(this);
			this.synapse.add(synTemp);
			neuron.synapseIn.add(synTemp);
		}
	}
	
	public void connect(NeuronOutput neuron, double weight)
	{
		Synapse synTemp = new Synapse(neuron,weight);
		synTemp.setIn(this);
		this.synapse.add(synTemp);
		neuron.synapseIn.add(synTemp);
	}
	
	public double fire()
	{
		//Send out on all synaptes
		for(int i = 0; i < this.synapse.size(); i++)
		{
			this.synapse.get(i).outVal = this.outVal;
		}
				
		return this.outVal;
	}
	
}