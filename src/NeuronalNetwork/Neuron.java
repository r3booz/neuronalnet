package NeuronalNetwork;
import java.util.ArrayList;


public class Neuron {
	public double threshold;
	public double outVal = 0.0;
	
	public ArrayList<Synapse> synapse = new ArrayList<Synapse>();
	public ArrayList<Synapse> synapseIn = new ArrayList<Synapse>();
	
	public Neuron(double threshold)
	{
		this.threshold = threshold;
	}

}