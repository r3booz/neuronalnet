package GUIInterface;
import java.awt.*;
import java.awt.event.*;

public class GUI extends Frame
{
	public NeuronalNetwork.Network net;
	public boolean exit = false;
	
	public GUI(NeuronalNetwork.Network net)
	{
		this.net = net;
		setTitle("NeuronalNetworkAI");
		addWindowListener(new GUIWindowListener());
		add(new DrawingPanel());
		setSize(500,500);
		setVisible(true);
	}
	
	class DrawingPanel extends Panel
	{
		public void paint(Graphics g)
		{
			int xo = 0;
			int yo = 0;
			
			g.setColor(Color.black);
			g.drawString("Input", xo+55-9, yo+30);
			for(int i = 0; i < GUI.this.net.input.size(); i++)
			{
				g.drawRect(xo+40, yo+40+(i*50), 40, 40);
				g.drawString("I:"+String.valueOf(GUI.this.net.input.get(i).outVal), xo+50, yo+65+(i*50));
				
				g.setColor(Color.green);
				for(int s = 0; s < GUI.this.net.input.get(i).synapse.size(); s++)
				{
					if (GUI.this.net.input.get(i).synapse.get(s).type==1)
					{
						for(int ol = 0; ol < GUI.this.net.layer.size();ol++)
						{
							for(int o = 0; o < GUI.this.net.layer.get(ol).hidden.size();o++)
							{
								if (GUI.this.net.input.get(i).synapse.get(s).hidden.equals(GUI.this.net.layer.get(ol).hidden.get(o)))
								{
									g.drawLine(xo+81, yo+60+(i*50), xo+49+(GUI.this.net.layer.size()*100), yo+57+(o*50));
									g.setColor(Color.black);
									g.drawString(String.valueOf(GUI.this.net.input.get(i).synapse.get(s).weight), (xo+81+xo+49+(GUI.this.net.layer.size()*100))/2, (yo+60+(i*50)+yo+57+(o*50))/2);
									g.setColor(Color.green);
								}
							}
						}
						
					}
					else if (GUI.this.net.input.get(i).synapse.get(s).type==2)
					{
						
					}
				}
				g.setColor(Color.black);
			}
			
			g.setColor(Color.blue);
			for(int i = 0; i < GUI.this.net.layer.size(); i++)
			{
				g.drawString("L"+String.valueOf(i+1), xo+65+(GUI.this.net.layer.size()*100), yo+30);
				
				for(int ii = 0; ii < GUI.this.net.layer.get(i).hidden.size(); ii++)
				{
					g.drawRect(xo+50+(GUI.this.net.layer.size()*100), yo+40+(ii*50), 40, 40);
					g.drawString("T:"+String.valueOf(GUI.this.net.layer.get(i).hidden.get(ii).threshold), xo+57+(GUI.this.net.layer.size()*100), yo+57+(ii*50));
					g.drawString("V:"+String.valueOf(GUI.this.net.layer.get(i).hidden.get(ii).outVal), xo+57+(GUI.this.net.layer.size()*100), yo+72+(ii*50));
					
					g.setColor(Color.green);
					for(int s = 0; s < GUI.this.net.layer.get(i).hidden.get(ii).synapse.size(); s++)
					{
						if (GUI.this.net.layer.get(i).hidden.get(ii).synapse.get(s).type==2)
						{
							for(int o = 0; o < GUI.this.net.output.size();o++)
							{
								if (GUI.this.net.layer.get(i).hidden.get(ii).synapse.get(s).output.equals(GUI.this.net.output.get(o)))
								{
									g.drawLine(xo+91+(GUI.this.net.layer.size()*100), yo+60+(ii*50), xo+150+(GUI.this.net.layer.size()*100), yo+57+(o*50));
									g.setColor(Color.black);
									g.drawString(String.valueOf(GUI.this.net.layer.get(i).hidden.get(ii).synapse.get(s).weight),(xo+91+(GUI.this.net.layer.size()*100))+(Math.abs((xo+91+(GUI.this.net.layer.size()*100))-(xo+150+(GUI.this.net.layer.size()*100)))/2),(yo+60+(ii*50))-(Math.abs((yo+60+(ii*50))-(yo+57+(o*50))))/2);
								}
							}
							
						}
						else if (GUI.this.net.layer.get(i).hidden.get(ii).synapse.get(s).type==1)
						{
							//TODO
						}
					}
					g.setColor(Color.blue);
				}
			}
			
			g.setColor(Color.red);
			g.drawString("Output", xo+165+(GUI.this.net.layer.size()*100)-14, yo+30);
			for(int i = 0; i < GUI.this.net.output.size(); i++)
			{
				g.drawRect(xo+150+(GUI.this.net.layer.size()*100), yo+40+(i*50), 40, 40);
				g.drawString("T:"+String.valueOf(GUI.this.net.output.get(i).threshold), xo+157+(GUI.this.net.layer.size()*100), yo+57+(i*50));
				g.drawString("V:"+String.valueOf(GUI.this.net.output.get(i).outVal), xo+157+(GUI.this.net.layer.size()*100), yo+72+(i*50));
			}
		}
	}
	
	class GUIWindowListener extends WindowAdapter
	{	
		public void windowClosing(WindowEvent e)
		{
			e.getWindow().dispose();
			GUI.this.exit = true;
		}
	}
}